from selenium import webdriver
from selenium.webdriver import ActionChains
import time
import unittest
import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from Automation.Analytics.pages.loginpage import LoginPage
from Automation.Analytics.pages.profilepage import ProfilePage
from Automation.Analytics.pages.appstorepage import AppStorePage
from Automation.Analytics.config.properties import properties
from Automation.Analytics.functions.utilities import util
import HtmlTestRunner

class LoginTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = webdriver.Chrome(properties.chrome_path)
        cls.driver.set_page_load_timeout(30)
        cls.driver.maximize_window()
        cls.driver.implicitly_wait(20)

    def test_1_login_valid(self):
        driver = self.driver
        driver.get(properties.url)
        driver.implicitly_wait(20)
        # login = LoginPage(driver)
        # login.enter_username(properties.username)
        # login.enter_password(properties.password)
        # login.click_login()
        # login.select_site()
        util.login(properties.username,properties.password)
        util.select_site()
        driver.implicitly_wait(20)

    def test_2_visitsanalytics(self):
        driver = self.driver
        visitsanl = AppStorePage(driver)
        visitsanl.select_visits_app()



    def test_3_profile(self):
        driver = self.driver
        profile = ProfilePage(driver)
        profile.select_profile()
        profile.select_logout()

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.close()
        cls.driver.quit()
        print("Driver execution completed Successfully")


if __name__ == '__main__':
    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output = 'C:\\Users\\Sriharsha.Desu\\PycharmProjects\\Analytics\\reports'))

